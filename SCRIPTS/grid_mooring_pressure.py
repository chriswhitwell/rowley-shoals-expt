"""
Maps the mooring temperature data into pressure coordinates
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d, PchipInterpolator

from mycurrents import oceanmooring as om

#########
Tfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_20sec.nc'
Pfile = 'processed_data/RowleyShoals_Gridded_Mooring_P.nc'

outfile = 'processed_data/RowleyShoals_Gridded_Mooring_TP.nc'

groups = [
	#'T150',
    #'T200',
    'T330',
    ]

bottom_depths = [
        #150,
        #201,
        331.]

mode = 'w'
#########

for group,bottom_depth in zip(groups, bottom_depths):
    T = om.from_netcdf(Tfile, 'Temperature', group=group)
    P = om.from_netcdf(Pfile, 'Pressure', group=group)

    # Interpolate temperature onto the pressure time coordinate here...
    new_t, new_y = T.interp(P.t)
    T = T.copy_like(new_t,new_y)

    assert P.Nt == T.Nt

    nt = P.Nt

    ######
    # Build a matrix for interpolating
    y = np.vstack([bottom_depth * np.ones((1,nt)), P.y.data])
    z = np.hstack([0, P.Z])

    # Build the interpolation object
    F = interp1d(z, y, axis=0, kind='linear', fill_value='extrapolate')
    #F = PchipInterpolator(z, y, axis=0, extrapolate=True)
    zout = T.Z
    z_hat = F(zout)
    z_hat[np.isnan(z_hat)] = 0.

    # Give the 
    T.is2Dz = True
    T.zvar = z_hat

    # Make sure the depth attributes are pointing down
    T.positive=u'down'

    ####
    # Write the output
    T.to_netcdf(outfile, group=group, mode=mode)
    mode ='a'
    #plt.figure()
    #plt.pcolormesh(P.tsec, zout, z_hat)
    #plt.colorbar()
    #plt.show()

print( 'Done')
