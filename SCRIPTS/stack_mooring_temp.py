"""
Stack mooring observations together and store
"""

from soda.dataio import netcdfio
from mycurrents import oceanmooring as om

import numpy as np

import pdb

def grid_temp(stationid, stationname, \
	varname, sampledt, endafter, outfile, mode, exclude=''):

    clip = False # Do not clip to bounding time region	
    #clip = True

    dbfile = 'RowleyShoals2019_Obs.db'
    tablename = 'observations'

    group = '%s'%stationid
    otherquery='StationID == "%s" AND time_end > "%s"'%\
	    (stationid, endafter)
    otherquery += exclude

    # Load the data
    temp = netcdfio.load_sql_ncstation(dbfile, stationname, varname,\
	    #otherquery=None)
	    otherquery=otherquery)

    # Sort by station names 
    # THis way the SBE37/9 go first (less memory)
    names = [ii.StationName for ii in temp]
    #sortnames = names.sort()
    # Sort by end time so that we are concatenating onto the longest array (hopefully...)
    #endtime = [ii.time_end.strftime('%Y-%m-%d %H:%M:%S') for ii in temp]
    endtime = np.array([np.datetime64(ii.time_end) for ii in temp])
    sortnames = [names[ii] for ii in np.argsort(endtime)[::-1]]

    if len(names) == 0:
        raise Exception('could not find any stations.')

    for ii, name in enumerate(sortnames):
        print( 'Stacking instrument: %s...'%name)
        for tt in temp:
            #print(tt.StationName, name)
            if tt.StationName == name:
                tmpdata = om.from_xray(tt)
                if sampledt>0.:
                    print( 'Resampling from dt = %3.1f to %3.1f'%(tmpdata.dt, sampledt))
                    tmp = tmpdata.resample(sampledt)
                    # Copy it back to an ocean mooring object
                    tmpdata = tmpdata.copy_like(tmp.t, tmp.y)

        if ii == 0:
            data = tmpdata
            data.verbose=True
        else:
            data.vstack( tmpdata, clip=clip)
        print(data.Z)
	

    #for ii, tt in enumerate(temp):
    #    print tt.StationName
    #    if ii == 0:
    #        data = om.from_xray(tt)
    #    else:
    #        data.vstack( om.from_xray(tt), clip=True )


    # Change the station name and id
    data.StationID = stationid
    data.StationName = stationname

    # Write to output netcdf
    data.to_netcdf(outfile, group=group, mode=mode)
    print( 'Done')

######
# Inputs
######
stationnames = [
	'T150_SBE56',
    'T200_SBE56',
    'T330_SBE56',
    ]

stationids = [
	'T150',
    'T200',
    'T330',
    ]

varname = 'temperature'

#varname = 'pressure'

#sampledt = 20.
sampledt = 0.

#endafter = "2019-05-01"
endafter = "2017-04-01" # Doesn't really matter here

# Exclude these from all gridding
exclude = ''

#exclude += r' AND StationName NOT LIKE "%SBE37%"'
exclude += r' AND StationName NOT LIKE "%4455%"'
exclude += r' AND StationName NOT LIKE "%822%"' # Bad 39plus data

mode = 'w'

#outfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_20sec.nc'
outfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_SBE56.nc'

#######

for stationname, stationid in zip(stationnames, stationids):
    grid_temp(stationid, stationname,\
	varname, sampledt, endafter, outfile, mode, exclude=exclude)
    mode='a'
