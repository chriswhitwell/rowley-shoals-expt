# Rowley Shoals 2019 Data Processing Sripts

May 2019

---

## Requirements

 - [**mycurrents**](https://bitbucket.org/mrayson/mycurrents): Contains classes
 for parsing different file formats data sets (wraps [**dolfyn**](https://github.com/lkilcher/dolfyn) package.)
 - [**soda**](https://github.com/mrayson/soda): Required by `mycurrents`.

## Installation

Install `soda` and `mycurrents` using pip:

`
pip install git+https://github.com/mrayson/soda.git@python3pip

pip install git+https://bitbucket.org/mrayson/mycurrents.git
`

---

This contains a howto for converting raw field data into useful netcdf files

## Step 1: Create a csv file with the file metadata

See the file `Data/KISSME_Instruments.csv` file for the necessary fields

## Step 2: Convert the csv to a db (sql) format

See: `SCRIPTS/convert_instrument_csv2sql.py`

(These first two steps may hopefully eventually become redundant when/if we use
an IMOS-like deployment database)

## Step 3: Convert the seabird data to netcdf

See: `SCRIPTS/parse_seabird.py`


## Step 4: Convert the raw RDI adcp data to netcdf

See: `SCRIPTS/parse_adcp.py`

**Note**: This needs to be called individually (some bug with not clearing memory?!?)
See `process_adcp.sh` and `SCRIPTS/list_adcp_files.py`

## Step 4a: QAQC adcp data and compute pressure coordinates

See: `SCRIPTS/qaqc_rdi_adcp.py`

## Step 5: Process the raw ADV data

TODO: Read ADV data and write to netcdf (currently saves as hdf)

A test script is here: `SCRIPTS/test_read_adv.py`

## Step 6: Create an sql database of the "raw" netcdf files

See: `SCRIPTS/update_database.py`

## Step 7: Grid the Seabird netcdf files into one array

See: `SCRIPTS/stack_mooring_data.py

Do the same for pressure and salinity

## Step 8: Interpolate the pressure onto temperature

See: `SCRIPTS/grid_mooring_pressure.py`

---

# Tutorial

T
